package com.example.winmaster.mediaplayer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToMusic(View view)
    {
        Intent intent = new Intent(this, MusicActivity.class);
        startActivity(intent);
    }

    public void goToPhotos(View view)
    {
        Intent intent = new Intent(this, PhotosActivity.class);
        startActivity(intent);
    }

    public void goToVideos(View view)
    {
        Intent intent = new Intent(this, VideosActivity.class);
        startActivity(intent);
    }
}

