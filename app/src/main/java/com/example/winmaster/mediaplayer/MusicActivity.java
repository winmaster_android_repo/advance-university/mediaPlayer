package com.example.winmaster.mediaplayer;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MusicActivity extends AppCompatActivity
{

    ListView listViewMusic;
    ArrayList<String> listOfMusic;
    ArrayList<String> listOfMusicPaths;
    Cursor cursor;
    MediaPlayer mediaPlayer;
    CustomAdapterMusic customAdapterMusic;
    TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        listViewMusic = findViewById(R.id.listViewMusic);
        listOfMusic = new ArrayList<>();
        listOfMusicPaths = new ArrayList<>();
        customAdapterMusic  = new CustomAdapterMusic();
        listViewMusic.setAdapter(customAdapterMusic);
        mediaPlayer = new MediaPlayer();

        cursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,
                MediaStore.Audio.Media.IS_MUSIC + " = 1",
                null,
                MediaStore.Audio.Media.TITLE + " ASC");

        String audioFiles= "";

        try
        {
            cursor.move(0);

            while (cursor.moveToNext())
            {
                audioFiles = "";
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                audioFiles+=title+" - "+artist+"\n";
                listOfMusic.add(audioFiles);
                listOfMusicPaths.add(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
            }

            customAdapterMusic.notifyDataSetChanged();
            cursor.moveToFirst();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "No music on device :(", Toast.LENGTH_SHORT).show();
        }


        //poprawić trochę zachowanie po kliknięciu, dodać efekt marque, może zamienić to na recycler view
        listViewMusic.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                if( mediaPlayer.isPlaying())
                {
                    mediaPlayer.stop();
                    //textViewTitle.setTextColor(getResources().getColor(R.color.colorBlack));
                }
                else
                {
                    try
                    {
                        mediaPlayer = null;
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDataSource(listOfMusicPaths.get(i));
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        //textViewTitle.setTextColor(getResources().getColor(R.color.colorAccent));
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(getApplicationContext(), "Something went wrong :(", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        mediaPlayer.stop();
        finish();
    }


    public class CustomAdapterMusic extends BaseAdapter
    {
        private LayoutInflater inflater = null;

        public int getCount()
        {
            return listOfMusic.size();
        }

        public Object getItem(int position)
        {
            return position;
        }

        public long getItemId(int position)
        {
            return position;
        }

        public View getView(final int position, View currentView, ViewGroup parent)
        {
            View myView;

            inflater = (LayoutInflater)
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (currentView == null)
            {
                currentView = inflater.inflate(R.layout.music_row, null);
            }
            myView = currentView;

            textViewTitle =  myView.findViewById(R.id.row_music_title);
            textViewTitle.setText(listOfMusic.get(position));
            return myView;
        }

    }
}
