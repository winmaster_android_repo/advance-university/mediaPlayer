package com.example.winmaster.mediaplayer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PhotoDetail extends AppCompatActivity
{

    ImageView imageViewPhoto;
    TextView textViewTitle;
    TextView textViewDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        imageViewPhoto =findViewById(R.id.imageViewPhoto);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewDescription = findViewById(R.id.textViewDescription);

        Intent intent = getIntent();
        String  photoLocation = intent.getStringExtra("photoLocation");
        String  photoTitle = intent.getStringExtra("photoTitle");
        String  photoDate = intent.getStringExtra("photoDate");

        textViewTitle.setText(photoTitle);
        textViewDescription.setText(photoDate);

        try
        {
            imageViewPhoto.setImageBitmap(BitmapFactory.decodeFile(photoLocation));
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }
}
