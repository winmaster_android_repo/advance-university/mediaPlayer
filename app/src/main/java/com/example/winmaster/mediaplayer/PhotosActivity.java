package com.example.winmaster.mediaplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PhotosActivity extends AppCompatActivity
{
    ListView listViewPhotos;
    ArrayList<String> listOfPhotosTitles;
    ArrayList<String> listOfPhotosDates;
    ArrayList<String> listOfPhotosLocation;
    CustomAdapter customAdapter;
    View viewStartTransition;
    Activity acivityBase;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        listViewPhotos = findViewById(R.id.listViewPhotos);
        listOfPhotosTitles = new ArrayList<>();
        listOfPhotosDates  = new ArrayList<>();
        listOfPhotosLocation = new ArrayList<>();

        customAdapter = new CustomAdapter();
        listViewPhotos.setAdapter(customAdapter);

        acivityBase = this;

        Cursor cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null,
                MediaStore.Images.Media.SIZE + " < 500000",
                null,
                MediaStore.Images.Media.DATE_ADDED + " DESC");

        try
        {
            cursor.move(0);

            while (cursor.moveToNext())
            {
                listOfPhotosTitles.add(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.TITLE)));
                String format = "MM-dd-yyyy HH:mm:ss";
                SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
                String date =  cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
                String dateTime = formatter.format(new Date(Long.parseLong(date)));
                listOfPhotosDates.add(dateTime);
                listOfPhotosLocation.add(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
            }

            customAdapter.notifyDataSetChanged();

            cursor.moveToFirst();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "There are no pictures on device :(", Toast.LENGTH_SHORT).show();
        }

        listViewPhotos.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Intent intent = new Intent(getApplicationContext(), PhotoDetail.class);
                intent.putExtra("photoLocation", listOfPhotosLocation.get(i));
                intent.putExtra("photoTitle", listOfPhotosTitles.get(i));
                intent.putExtra("photoDate", listOfPhotosDates.get(i));

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(acivityBase, viewStartTransition  ,"simple_activity_transition");
                startActivity(intent, options.toBundle());
            }
        });

    }

    public class CustomAdapter extends BaseAdapter
    {
        private LayoutInflater inflater = null;

        public int getCount()
        {
            return listOfPhotosTitles.size();
        }

        public Object getItem(int position)
        {
            return position;
        }

        public long getItemId(int position)
        {
            return position;
        }

        public View getView(final int position, View currentView, ViewGroup parent)
        {
            View myView;

            inflater = (LayoutInflater)
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (currentView == null)
            {
                currentView = inflater.inflate(R.layout.list_row, null);
            }
            myView = currentView;
            viewStartTransition = myView;

            TextView textViewTitle =  myView.findViewById(R.id.row_title);
            textViewTitle.setText(listOfPhotosTitles.get(position));
            TextView textViewDescription =  myView.findViewById(R.id.row_descr);
            textViewDescription.setText(listOfPhotosDates.get(position));
            ImageView imageViewPhoto = myView.findViewById(R.id.row_image);
            try
            {
               File imgFile  = new File(listOfPhotosLocation.get(position));
               imageViewPhoto.setImageBitmap(BitmapFactory.decodeFile(imgFile.getAbsolutePath()));
            }
            catch (Exception e)
            {
                imageViewPhoto.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.appicon));
            }

            return myView;
        }

    }
}
