package com.example.winmaster.mediaplayer;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class VideosActivity extends AppCompatActivity
{
    ListView listViewVideos;
    ArrayList<String> listOfVideosTitles;
    ArrayList<String> listOfVideosDates;
    ArrayList<String> listOfVideosLocation;
    CustomAdapterVideo customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);

        listViewVideos = findViewById(R.id.listViewVideos);
        listOfVideosTitles = new ArrayList<>();
        listOfVideosDates  = new ArrayList<>();
        listOfVideosLocation = new ArrayList<>();

        customAdapter = new CustomAdapterVideo();
        listViewVideos.setAdapter(customAdapter);

        Cursor cursor = getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                null,
                null,
                null,
                MediaStore.Video.Media.TITLE + " ASC");


        try
        {
            cursor.move(0);

            while (cursor.moveToNext())
            {
                listOfVideosTitles.add(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.TITLE)));
                String format = "MM-dd-yyyy HH:mm:ss";
                SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
                String date =  cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATE_TAKEN));
                String dateTime = formatter.format(new Date(Long.parseLong(date)));
                listOfVideosDates.add(dateTime);
                listOfVideosLocation.add(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA)));
            }

            customAdapter.notifyDataSetChanged();

            cursor.moveToFirst();

            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "There are no videos on device :(", Toast.LENGTH_SHORT).show();
        }

        listViewVideos.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(listOfVideosLocation.get(i)));
                intent.setDataAndType(Uri.parse(listOfVideosLocation.get(i)), "video/mp4");
                startActivity(intent);
            }
        });
    }

    public class CustomAdapterVideo extends BaseAdapter
    {
        private LayoutInflater inflater = null;

        public int getCount()
        {
            return listOfVideosTitles.size();
        }

        public Object getItem(int position)
        {
            return position;
        }

        public long getItemId(int position)
        {
            return position;
        }

        public View getView(final int position, View currentView, ViewGroup parent)
        {
            View myView;

            inflater = (LayoutInflater)
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (currentView == null)
            {
                currentView = inflater.inflate(R.layout.list_row, null);
            }
            myView = currentView;

            TextView textViewTitle =  myView.findViewById(R.id.row_title);
            textViewTitle.setText(listOfVideosTitles.get(position));
            TextView textViewDescription =  myView.findViewById(R.id.row_descr);
            textViewDescription.setText(listOfVideosDates.get(position));
            ImageView imageViewPhoto = myView.findViewById(R.id.row_image);

            imageViewPhoto.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.appicon));

            return myView;
        }
    }
}
